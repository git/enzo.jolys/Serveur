from crypt import methods
from flask import Blueprint, render_template,request,redirect ,url_for
import flask
from flask_login import login_required,current_user
from requests import session
from .models import DataPicture
from . import db


views = Blueprint('views',__name__)


@views.route('/',methods=['GET','POST'])
@login_required
def home():
    if request.method == 'POST':
        var = request.form.get('data')

        if len(var) < 1 :
            pass
        else :
            new_data = DataPicture(data=var,user_id=current_user.id)
            db.session.add(new_data)
            db.session.commit()
            print("data add")

    return render_template("home.html",user=current_user)


@views.route('/acceuil',methods=['GET'])
def acceuil():
    return render_template("acceuil.html")


@views.route('/delete_all_picture')
@login_required
def delete_all_picture():
    var = 1
    data = DataPicture.query.get(var) 
    
    while data :
        db.session.delete(data)
        var+=1
        data = DataPicture.query.get(var)
    db.session.commit()

    return redirect(url_for('views.home'))


@views.route('/api/config',methods=['GET'])
@login_required
def api():
    file = current_user.config_pi
    file ="test.txt"
    return flask.send_file("config/"+file)