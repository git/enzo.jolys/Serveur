from flask import Blueprint,render_template,request,flash, redirect ,url_for
from .models import User
from werkzeug.security import generate_password_hash,check_password_hash
from . import db
from flask_login import login_user,login_required,logout_user,current_user

auth = Blueprint('auth',__name__)


@auth.route('/login',methods=['GET','POST'])
def login():
    if request.method == 'POST':
        email = request.form.get('email')
        password = request.form.get('password')

        user = User.query.filter_by(email=email).first()
        if user:
            if check_password_hash(user.password,password):
                #logged
                print("logged")
                login_user(user,remember=True)
                return redirect(url_for('views.home'))
            else :
                #Password wrong
                print("Password wrong")
                pass
        else :
            #Not find user
            print("User don't exist")
    
    return render_template("login.html")


@auth.route('/seconnecter',methods=['GET','POST'])
def seconnecter():
    return render_template("seconnecter.html")


@auth.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('auth.login'))

@auth.route('/sign-up',methods=['GET','POST'])
def sign_up():
    if request.method == 'POST' :
        email = request.form.get('email')
        firstName = request.form.get('firstName')
        password = request.form.get('password')
        password2 = request.form.get('password2')

        #Check if exist user
        user = User.query.filter_by(email=email).first()
        if user :
            print("User already exist")
            return redirect(url_for('auth.login'))
     
        elif len(email) < 4 :
            pass
        elif len(firstName) < 2 :
            pass
        elif password != password2 :
            return "Wrong"
        elif len(password) < 7 :
            return "Wrong"
        else :
            new_user = User(email=email,firstName=firstName,password=generate_password_hash(password,method='sha256'),config_pi="rien")
            db.session.add(new_user)
            db.session.commit()
            login_user(user,remember=True)
            return redirect(url_for('auth.login'))
    else :
        return render_template("sign-up.html")



# Utiliser flash pour envoyer des petits message (error ou sucess)!!