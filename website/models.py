from enum import unique
from . import db
#Import db from init.py
from flask_login import UserMixin
from sqlalchemy.sql import func

class DataPicture(db.Model):
    id = db.Column(db.Integer,primary_key=True)
    data = db.Column(db.String(100))
    date = db.Column(db.DateTime(timezone=True),default=func.now())

    #Foreign Key of User
    user_id = db.Column(db.Integer,db.ForeignKey('user.id'))


class User(db.Model,UserMixin):

    id = db.Column(db.Integer,primary_key=True)
    email = db.Column(db.String(100),unique=True)
    password = db.Column(db.String(100))
    firstName = db.Column(db.String(100))

    config_pi = db.Column(db.String(100))

    #lien au niveau de html voir home.html
    pictures = db.relationship('DataPicture')



