from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from os import path
from flask_login import LoginManager, login_manager

db = SQLAlchemy()
DB_NAME = "database.db"

def create_app():
    app = Flask(__name__,static_url_path='/static')
    app.config['SECRET_KEY'] = 'toto'
    app.config['SQLALCHEMY_DATABASE_URI'] = f'sqlite:///{DB_NAME}'
    db.init_app(app)

    from .views import views
    #import les routes de views

    from .auth import auth
    #import les routes de auth

    app.register_blueprint(views,url_prefix='/')
    app.register_blueprint(auth,url_prefix='/')
    #Préfixes pour accéder à ces routes

    from .models import User,DataPicture
    create_database(app)

    #redirect on the page login if not loggin
    login_manager = LoginManager()
    login_manager.login_view = 'auth.login'
    #what app we use 
    login_manager.init_app(app)

    #how we load a User ( model User and the primary key id)
    @login_manager.user_loader
    def load_user(id):
        return User.query.get(int(id))

    return app



def create_database(app):
    if not path.exists('website/' + DB_NAME):
        with app.app_context():
            db.create_all()
            print('Created Database!')






