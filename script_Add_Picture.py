import sqlite3
import sys

def info():
    print("Info Argument: Name_Picture Name_User")

def calculUser(cur):
    id_free = 1

    while True :
        cur.execute('SELECT id FROM Data_Picture WHERE id == ?',(str(id_free)))
        tmp = cur.fetchall()

        if len(tmp) == 0 :
            break
        id_free+=1

    return id_free

arg = sys.argv

if len(arg) != 3 :
    info()
    exit(1)

try :
    conn = sqlite3.connect('instance/database.db')
    cur = conn.cursor()
    print("Connection : OK")
       
    id_free = calculUser(cur)

    nom_picture = arg[1]
    user = arg[2]

    cur.execute('INSERT INTO Data_Picture(id,data,user_id) VALUES(?,?,?)',(id_free,nom_picture,user))
    conn.commit()
    print("Picture add")

    cur.close()
    conn.close()
except sqlite3.Error as error :
    print(error)
 